#!/bin/sh

kind delete cluster
./01-kind-registry.sh
./02-kind.sh
./03-postgres-operator.sh
./04-tag-push.sh
./05-application.sh
./06-kubectl-config-view.sh
./07-portforward-postgres.sh
