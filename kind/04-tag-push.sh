#docker tag liquibase-demo-app:0.0.1-SNAPSHOT localhost:5000/imageliquibase-demo-app:0.0.1-SNAPSHOT
#docker push localhost:5000/liquibase-demo-app:0.0.1-SNAPSHOT

# Tag own image
docker tag liquibase-demo-app:1.0.0 localhost:5000/liquibase-demo-app:1.0.0
docker push localhost:5000/liquibase-demo-app:1.0.0

docker tag liquibase-demo-app-liquibase:1.0.0 localhost:5000/liquibase-demo-app-liquibase:1.0.0
docker push localhost:5000/liquibase-demo-app-liquibase:1.0.0
