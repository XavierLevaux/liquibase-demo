kubectl apply -f postgres-operator/configmap.yaml                     # configuration
kubectl apply -f postgres-operator/operator-service-account-rbac.yaml # identity and permissions
kubectl apply -f postgres-operator/postgres-operator.yaml             # deployment
kubectl apply -f postgres-operator/api-service.yaml                   # operator API to be used by UI

echo "Let some time for postgresql kind to be available"
sp="/-\|"
try_num=0
printf ' '
until [ "$try_num" -ge 30 ]; do
  # get all names and concatenate them with comma
  NAMES="$(kubectl api-resources --api-group=acid.zalan.do -o name --verbs list | tr '\n' ',')"
  #echo "$NAMES"

  #if "$(echo $NAMES | grep 'postgresqls.acid.zalan.do')"; then
  if echo "$NAMES" | grep 'postgresqls.acid.zalan.do'; then
    echo "api resource found!"
    break
  fi
  try_num=$((try_num + 1))
  printf '\b%.1s' "$sp"
  sp=${sp#?}${sp%???}
  #sleep 3
done
echo ""

kubectl apply -f postgres-operator/minimal-postgres-manifest.yaml
