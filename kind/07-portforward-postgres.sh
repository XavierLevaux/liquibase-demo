#!/bin/sh
PGUSER=$(kubectl get secret postgres.acid-minimal-cluster.credentials -o 'jsonpath={.data.username}' | base64 -d)
PGPASSWORD=$(kubectl get secret postgres.acid-minimal-cluster.credentials -o 'jsonpath={.data.password}' | base64 -d)
echo "postgres username: $PGUSER"
echo "postgres password: $PGPASSWORD"

export PGMASTER=$(kubectl get pods -o jsonpath={.items..metadata.name} -l application=spilo,cluster-name=acid-minimal-cluster,spilo-role=master)
echo "postgres PGMASTER: $PGMASTER"
# set up port forward
kubectl port-forward $PGMASTER 6432:5432