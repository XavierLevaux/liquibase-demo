#!/bin/sh
set -o errexit

kind create cluster --config kind-local-registry.yaml
docker network connect "kind" "kind-registry"