-- select 120, pg_sleep(120);

create table customer
(
    customer_id varchar(255) not null
        constraint customer_pkey
            primary key,
    name varchar(255)
);

alter table customer owner to evs_demo_user;
