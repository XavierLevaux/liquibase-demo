create table topic_offset
(
	topic_id varchar(255) not null
		constraint topic_offset_pkey
			primary key,
	last_offset bigint not null,
	version integer not null
);

alter table topic_offset owner to evs_demo_user;
