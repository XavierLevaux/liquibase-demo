create table stock
(
	stock_id varchar(255) not null
		constraint stock_pkey
			primary key,
	product_id varchar(255),
	quantity integer
);

alter table stock owner to evs_demo_user;
