package be.xl.liquibase.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@ContextConfiguration(initializers = {DemoApplicationTests.Initializer.class})
class DemoApplicationTests {

   @ClassRule
   public static TestPostgresqlContainer postgreSQLContainer = TestPostgresqlContainer
       .getInstance().withUsername("evs_demo_user");

   @BeforeAll
   static void before() {
      postgreSQLContainer.start();
   }

   @AfterAll
   static void after() {
      postgreSQLContainer.stop();
   }

   @Test
   void contextLoads() {
   }

   public static class Initializer implements
       ApplicationContextInitializer<ConfigurableApplicationContext> {

      public Initializer() {
      }

      @Override
      public void initialize(final ConfigurableApplicationContext configurableApplicationContext) {
         TestPropertyValues.of("spring.datasource.url=" + postgreSQLContainer.getJdbcUrl())
             .applyTo(configurableApplicationContext.getEnvironment());
         TestPropertyValues.of("spring.datasource.username=" + postgreSQLContainer.getUsername())
             .applyTo(configurableApplicationContext.getEnvironment());
         TestPropertyValues.of("spring.datasource.password=" + postgreSQLContainer.getPassword())
             .applyTo(configurableApplicationContext.getEnvironment());
      }
   }
}
